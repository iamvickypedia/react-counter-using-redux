# [react-counter-using-redux](https://gitlab.com/iamvickypedia/react-counter-using-redux#react-counter-using-redux)

### This react App consists of a basic/advanced Counter using Redux store.

#### Installation

```bash
npm install
```

#### Start

```bash
npm start
```

The step value can be dynamic which can be altered from the input area.
