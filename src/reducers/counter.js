const counterReducer = (state = 0, action) => {
  switch (action.type) {
    case "INCREMENT":
      return state + action.nr;
    case "DECREMENT":
      return state - action.nr;
    default:
      return state;
  }
};

export default counterReducer;
