export const increment = nr => {
  return {
    type: "INCREMENT",
    nr: nr
  };
};
export const decrement = nr => {
  return {
    type: "DECREMENT",
    nr: nr
  };
};
