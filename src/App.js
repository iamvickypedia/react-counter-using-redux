import React from "react";
import "./App.css";
import Counter from "./components/counter";

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">React Counter</header>
        <Counter />
      </div>
    );
  }
}

export default App;
