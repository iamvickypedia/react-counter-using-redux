import React, { useState } from "react";
import { increment, decrement } from "../actions";
import { useSelector, useDispatch } from "react-redux";

function Counter() {
  const [inc, setInc] = useState(1);
  const counter = useSelector(state => state.counter);
  const logged = useSelector(state => state.logged);
  const dispatch = useDispatch();
  return (
    <p>
      <h1>Counter {counter}</h1>
      <p>
        <input
          type="text"
          name="inc_val"
          value={inc}
          onChange={e => setInc(parseInt(e.target.value))}
        />
      </p>
      <button onClick={() => dispatch(increment(inc))}>+</button>
      <button onClick={() => dispatch(decrement(inc))}>-</button>
      {logged ? <h3>Valuable Info</h3> : <h3>Permission Denied</h3>}
    </p>
  );
}

export default Counter;
